import asyncio
from dataclasses import dataclass
from pathlib import Path
from rich import print
from datetime import datetime, timedelta
from enum import IntEnum, auto
from tasktools.taskloop import TaskLoop
from .settings_manager import SettingsFile
#from .read_data import read_csv, Mode, load_stations
from .dbsteps import DBStep, DBSend
from .queue_steps import QueueSend
from data_rdb import Rethink_DBS
from rethinkdb import RethinkDB
from networktools.time import get_datetime_di
from collector_mq_queue.manager import AMQP_Manager

from typing import Optional, List  # python 3.9.2
import pytz
local = pytz.timezone("America/Santiago")

rdb = RethinkDB()

KEY = "DT_GEN"
FILTER_OPT = {'left_bound': 'open', 'index': KEY}
DAYS = 7.0
HOURS = 24.0
DATA_LIMIT = 100_000
STEP = 5


def tramos(inicio, final, seconds=STEP):
    lista = []
    tiempo = inicio
    tiempo_last = inicio + timedelta(seconds=seconds)

    while tiempo <= final:
        tiempo_last = tiempo + timedelta(seconds=seconds)
        lista.append((tiempo, tiempo_last))
        tiempo = tiempo_last
    return lista


async def gen(lista):
    for i in lista:
        yield i


@dataclass
class QueueWorker:
    settings: SettingsFile

    @property
    def days(self):
        """
        Fecha final, dias hacia atras
        """
        return self.settings.days or DAYS

    @property
    def hours(self):
        """
        Fecha final, dias hacia atras
        """
        return self.settings.hours or HOURS

    @property
    def destiny(self):
        return self.settings.destiny

    @property
    def origin(self):
        return self.settings.origin

    @property
    def sleep(self):
        return 1

    @property
    def days_seconds(self):
        return self.days*24*60*60

    def start_date(self):
        naive = datetime.now()  # self.settings.start_date() or datetime(2023, 4, 14, 12)
        local_dt = local.localize(naive, is_dst=None)
        utc_dt = local_dt.astimezone(pytz.utc)
        # CONSTANTES
        print("Start date", utc_dt)
        return utc_dt - timedelta(seconds=10)

    def now_date(self):
        naive = datetime.now()  # self.settings.start_date() or datetime(2023, 4, 14, 12)
        local_dt = local.localize(naive, is_dst=None)
        utc_dt = local_dt.astimezone(pytz.utc)
        # CONSTANTES
        return utc_dt

    async def transfer_data(self,
                            di: datetime,
                            control_origin: DBStep,
                            origin: Optional[Rethink_DBS],
                            control_destiny: DBSend,
                            destiny: Optional[AMQP_Manager],
                            tables: List[str],
                            *args,
                            **kwargs):

        if not di:
            control = True
            while control:
                try:
                    di = self.start_date()
                    control = False
                except:
                    print("Failed to read settings")
                    await asyncio.sleep(1)

        if self.settings.destiny_change:
            control_destiny = QueueSend.CREATE
            self.settings.destiny_change = False
            if destiny:
                del destiny
                destiny = None

        if self.settings.origin_change:
            control_origin = DBStep.CREATE
            self.settings.origin_change = False
            if origin:
                await origin.close()
                del origin
                origin = None

        raw_df = self.now_date()
        lista_tramos = tramos(di, raw_df, seconds=STEP)
        df = rdb.iso8601(raw_df)

        # create instances
        if control_origin == DBStep.CREATE:
            opts = self.origin.dict()
            kwargs["origin_db"] = self.origin
            origin = Rethink_DBS(**opts)
            control_origin = DBStep.CONNECT

        if control_destiny == QueueSend.CREATE:
            opts = self.settings.params()
            kwargs["destiny_db"] = self.destiny  # TODO: Check
            destiny = AMQP_Manager(n_chann=10, **opts)
            control_destiny = QueueSend.CONNECT

        if control_origin == DBStep.CONNECT:
            try:
                await asyncio.wait_for(
                    origin.async_connect(),
                    timeout=10)
                await origin.list_dbs()
                await origin.list_tables()
                tables = [n for n in origin.tables.get("collector") if
                          n != "log"]
                control_origin = DBStep.COLLECT
            except asyncio.TimeoutError as te:
                control_origin = DBStep.CONNECT

        if control_destiny == DBSend.CONNECT:
            try:
                await asyncio.wait_for(
                    destiny.start(),
                    timeout=5)
                control_destiny = QueueSend.SEND
            except asyncio.TimeoutError as te:
                control_destiny = QueueSend.CONNECT

        if control_origin == DBStep.COLLECT and control_destiny == QueueSend.SEND:
            tablas = set(tables) - {"log"}
            async for table_name in gen(tablas):
                destiny.add_queue(table_name)
                try:
                    async for (ndi, ndf) in gen(lista_tramos):
                        cursor = await origin.get_data_filter(
                            table_name,
                            [ndi, ndf],
                            FILTER_OPT,
                            KEY,
                            fields=[
                                "DELTA_TIME", "ECEF", "POSITION_VCV",
                                "TIME", "DT_GEN", "DT_RECV"])

                        if cursor:
                            if table_name == "CIFU_GSOF":
                                print(cursor)
                            lista_oid = [c.get('id') for c in cursor]
                            if self.settings.not_debug:
                                if table_name != "log":
                                    # print(
                                    #     f"{table_name} Cursor not empty", len(cursor))

                                    for data in cursor:
                                        try:
                                            await asyncio.wait_for(
                                                destiny.publish(data,
                                                                table_name),
                                                3)
                                        except TimeoutError as te:
                                            print(
                                                "No se pudo enviar data en 3secs")
                            else:
                                print(f"Sending queue....(debug mode) {table_name}",
                                      len(lista_oid))
                            di = ndf - timedelta(seconds=3)
                        else:
                            print(
                                f"Cursor empty {table_name} for {ndi} to {ndf}")

                except asyncio.exceptions.CancelledError as es:

                    print(f"Falla al consultar tabla {table_name}",
                          [di, df])

                    raise es
        # obtener desde origin por cada tabla, los datos anteriores 'days' hacia
        # atras
        # guardar en destino cada dato en su tabla correspondiente
        await asyncio.sleep(self.sleep)
        return [
            di,
            control_origin,
            origin,
            control_destiny,
            destiny,
            tables,
            *args
        ], kwargs

    def run(self):
        loop = asyncio.get_event_loop()
        control_origin = DBStep.CREATE
        control_destiny = QueueSend.CREATE
        tables = []
        di = None
        task = TaskLoop(self.transfer_data, [di, control_origin, None,
                                             control_destiny, None,
                                             tables], {})
        task.create()
        if not loop.is_running():
            loop.run_forever()
