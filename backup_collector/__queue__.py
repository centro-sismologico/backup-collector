import typer
import asyncio
from pathlib import Path
from rich import print
from .settings_manager import QueueSettingsFile
from .queue_worker import QueueWorker

app = typer.Typer()


@app.command()
def run(settings: Path, debug: bool = True):
    if settings.exists():
        loop = asyncio.get_event_loop()
        sfile = QueueSettingsFile(
            settings,
            unique=False,
            not_debug=not debug)
        queue_w = QueueWorker(
            settings=sfile)
        queue_w.run()
        if not loop.is_running():
            loop.run_forever()
    else:
        print("No existe")


if __name__ == "__main__":
    app()
