from enum import IntEnum, auto


class QueueSend(IntEnum):
    CREATE = auto()
    CONNECT = auto()
    SEND = auto()
