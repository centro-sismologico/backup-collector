                                             
                            BACKUP COLLECTOR

                                 David
                                             


Table of Contents
                 

.. 1. debug mode en command line
.. 2. Comando de consola
.. 3. Servicio para systemd
1. TODO Por hacer
.. 1. TODO logging
.. 2. TODO test unitarios de cada parte
.. 3. TODO mayor documentaci�n


Este software realiza la gesti�n de los datos que almacena collector en
una base de datos /fuente/.

Toma todos los datos a partir de una /fecha l�mite/ hac�a atr�s desde
una fecha de inicio. Por cada tabla (estaci�n) de la base de datos
definida en el archivo /settings.toml/ se lee y copia hacia la tabla
correspondiente en /destino/.

Esto se realiza de forma peri�dica seg�n lo que defina al iniciar la
aplicacion /servicio/, permite cambiar en vivo parametros como la fecha
de inicio, configuraci�n de /origen/ o /destino/ seg�n corresponda.

Un archivo de ejemplo se podr� encontrar en:

     
  cat ./settings/settings.toml
     

Para inicializar el servicio bastar� correr.

     
  poetry run python -m backup_collector settings/settings.toml
     

Adem�s, el sistema provee actualizacion en vivo de diferentes
caracter�sticas que definen al comportamiento del servicio, as� como
tambi�n setear el origen o el destino sin detener la operaci�n.

Bastar� modificar el archivo, cambiar sus valores de manera adecuada y
el sistema se ajustar� en la pr�xima iteraci�n.


0.1 debug mode en command line
                              

  Se definido que en el comando, por defecto estar� en modo debug, para
  activar modo producci�n a�adir * no-debug*.

       
    poetry run python -m backup_collector settings/settings.toml --no-debug
       

  O bien, ser explicito para estar en *modo debug*

       
    poetry run python -m backup_collector settings/settings.toml --debug
       

  El modo debug no guarda en destino y no borra en origen los
  datos. Solo lee desde el origen y muestra cuantos datos se han le�do.


0.2 Comando de consola
                      

  Asimismo, en vez de llamar al int�rprete python, es posible llamar al
  comando *backup_collector* que permitir� realizar la activaci�n del
  servicio.

  En modo debug.

       
    backup_collector settings/settings.toml  --debug
       

  En modo producci�n

       
    backup_collector settings/settings.toml  --no-debug
       


0.3 Servicio para systemd
                         

  En producci�n, alg�n servidor adecuado, sera posible activar un
  servicio systemd para /backup_collector/.

  Para eso sera necesario entrar a *./backup_collector/services* y
  ejecutar el script *build_service.sh*, modificando el valor de la
  variable *PROJECT_PATH* como corresponda.

       
    cat ./backup_collector/services/build_service.sh
       

  Con esto estar� disponible para el administrador activar o desactivar
  *backup_collector*,

       
    systemctl status --user backup_collector 
    systemctl start --user backup_collector 
    systemctl stop --user backup_collector 
    systemctl restart --user backup_collector 
       

  As� como monitorear la actividad.

       
    journalctl --user -u backup_collector -f
       

  Las variables de ambiente que utiliza se pueden definir aqu�, como
  setear la ruta al archivo *SETTINGS_FILE* o definir el modo *debug* o
  no.

       
    cat ./backup_collector/services/backup_collector.env
       


1 TODO Por hacer
                

  Lo siguiente queda por hacer


1.1 TODO logging
                


1.2 TODO test unitarios de cada parte
                                     


1.3 TODO mayor documentaci�n
                            
